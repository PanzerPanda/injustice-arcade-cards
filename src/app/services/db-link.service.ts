import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Card } from '../models/card';

@Injectable({
  providedIn: 'root'
})
export class DbLinkService {

  cardCollection: AngularFirestoreCollection<Card>;

  constructor(private afs: AngularFirestore) {
    this.cardCollection = this.afs.collection<Card>('cards');
  }

  addCard(card: Card){
    console.log('adding card', card.id);
    console.log(card);
    this.cardCollection.doc(card.id.toString()).set(card);
  }

  getCard(cardNo: string): AngularFirestoreDocument<Card> {
    return this.cardCollection.doc(cardNo);
  }

  deleteCard(cardNo: string) {
    this.cardCollection.doc(cardNo).delete();
  }

  getImage(imageID: string): AngularFirestoreDocument<Card> {
    const imageRef = this.afs.collection('cards');
    return imageRef.doc(imageID);
  }
}
