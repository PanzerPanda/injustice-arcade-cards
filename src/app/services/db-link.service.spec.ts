import { TestBed } from '@angular/core/testing';

import { DbLinkService } from './db-link.service';

describe('DbLinkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DbLinkService = TestBed.get(DbLinkService);
    expect(service).toBeTruthy();
  });
});
