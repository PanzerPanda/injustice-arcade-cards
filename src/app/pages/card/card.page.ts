import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Card } from 'src/app/models/card';
import { DbLinkService } from 'src/app/services/db-link.service';
import { Observable } from 'rxjs/internal/Observable';;
import { take } from 'rxjs/internal/operators/take';
import { switchMap } from 'rxjs/internal/operators/switchMap';

@Component({
  selector: 'app-card',
  templateUrl: './card.page.html',
  styleUrls: ['./card.page.scss'],
})
export class CardPage implements OnInit {

  card$: Observable<Card>;

  constructor(private route: ActivatedRoute, private navCtrl: NavController, private db: DbLinkService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('id')) {
        this.navCtrl.navigateBack('list');
        return;
      }
      this.card$ = this.db.getCard(paramMap.get('id')).valueChanges();
    });
  }

  deleteCard(cardId: string) {
    console.log(cardId);
    this.db.deleteCard(cardId.toString());
  }

}
