import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Card } from 'src/app/models/card';
import { DbLinkService } from 'src/app/services/db-link.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  cards$: Observable<any>;
  
  constructor(private db: DbLinkService) { 
  }

  ngOnInit() {
    this.cards$ = this.db.cardCollection.valueChanges();
  }

}
