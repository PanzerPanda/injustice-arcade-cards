import { Component, OnInit } from '@angular/core';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DbLinkService } from 'src/app/services/db-link.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-management',
  templateUrl: './management.page.html',
  styleUrls: ['./management.page.scss'],
})
export class ManagementPage implements OnInit {
  
  card: FormGroup;
  photo: SafeResourceUrl = '';

  constructor(private fb: FormBuilder, private db: DbLinkService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.card = this.fb.group({
      id: ['', Validators.required],
      front: [''],
      back: [''],
      name: ['', Validators.required],
      rarity: ['gold', Validators.required],
      type: ['solo', Validators.required],
      series: [2, Validators.required],
      stars: ['', Validators.required],
      holo: [false]
    });
  }

  async takePicture() {
    const image = await Plugins.Camera.getPhoto({
      quality: 100,
      allowEditing: false,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });

    this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl));
  }

  onSubmit() {
    this.db.addCard(this.card.value);
  }
}
