export interface Card {
    id: number;
    front: string;
    back: string;
    name: string;
    rarity: string;
    type: string;
    series: number;
    stars: number;
    holo: boolean;
}